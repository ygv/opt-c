##### For all Log groups which has NeverExpire
import boto3

cloudwatch = boto3.client("logs", 'us-east-1')

def cloudwatch_set_retention(days):
    retention = days

    paginator = cloudwatch.get_paginator('describe_log_groups')
    for page in paginator.paginate():
        for group in page['logGroups']:
            print(group)
            if "retentionInDays" not in group:
                print(f"Retention needs to be updated for: {group['logGroupName']}")
                cloudwatch.put_retention_policy(
                    logGroupName=group["logGroupName"], retentionInDays=retention
                )
            else:
                print(
                    f"CloudWatch Loggroup: {group['logGroupName']} already has the specified retention of {group['retentionInDays']} days."
                )

def lambda_handler(event, context):
    # TODO implement
    cloudwatch_set_retention(90)

##############################################################################################################################

##### For except some Log groups 
   
import boto3

cloudwatch = boto3.client("logs", 'us-east-1')

def cloudwatch_set_retention(days):
    retention = days

    paginator = cloudwatch.get_paginator('describe_log_groups')
    for page in paginator.paginate():
        for group in page['logGroups']:
            print(group)
            if ("test_lambda" not in group['logGroupName']) and ("test_function" not in group['logGroupName']):
                if "retentionInDays" not in group or group["retentionInDays"] != retention:
                    print(f"Retention needs to be updated for: {group['logGroupName']}")
                    cloudwatch.put_retention_policy(
                        logGroupName=group["logGroupName"], retentionInDays=retention
                    )
                else:
                    print(
                        f"CloudWatch Loggroup: {group['logGroupName']} already has the specified retention of {group['retentionInDays']} days."
                    )

def lambda_handler(event, context):
    # TODO implement
    cloudwatch_set_retention(90)
    
