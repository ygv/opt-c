import json
import ipaddress

ip_data = {
    248607199015:["10.6.90.0/23","10.6.92.0/23","10.6.94.0/23"],
    915180751011:["10.0.1.0/24","10.0.2.0/24","10.0.3.0/24"],
    360297428551:["10.6.98.0/23","10.6.100.0/23","10.6.102.0/23"]
}
AccountNames = {"dev":248607199015, "shared":915180751011, "prod":360297428551,}
subnet = ["a","b","c"]

def lambda_handler(event, context):
    # TODO implement
    event_passd = event
    http_data=event['requestContext']['http']
    http_data_method=http_data['method']
    form_style="""input[type=text], select {
  width: 20%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}
 
input[type=submit] {
  width: 35%;
  background-color: grey;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}
 
input[type=submit]:hover {
  background-color: blue;
}
 
div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>"""
    form_html= """<html><body><title>Provide IPAddress</title><style>{form_style}</style><h2>IPAddress Validation</h2><form action="get_request"><label for="fname">Enter IPAddress</label>
    <input type="text" id="sname" name="ipAddress" placeholder="Write IPAddress here.."><br><input type="submit" value="Submit"></form></form></body></html>""".format(form_style=form_style)
    if http_data_method == 'GET':
        try:
            ipAddress = event['queryStringParameters']['ipAddress']
        except KeyError:
            return {
        'statusCode': 200,
        'headers': {'Content-Type': 'text/html'},
        'body': form_html}

    data=json.dumps(event_passd)
    string = ""
    check = True
    if ipAddress.count('.') != 3:
        check = False
    else:
        ip_list = list(map(str, ipAddress.split('.')))

        # check range of each number between periods
        for element in ip_list:
            if int(element) < 0 or int(element) > 255 or (element[0]=='0' and len(element)!=1):
                check = False

    if check == True:
        for i in ip_data.values():
            for j in i:
                if ipaddress.ip_address(ipAddress) in ipaddress.ip_network(j):
                    number = list(ip_data.keys())[list(ip_data.values()).index(i)];
                    name = list(AccountNames.keys())[list(AccountNames.values()).index(number)]
                    subnetname = "Private-Subnet-us-east-1"+subnet[i.index(j)]
                    string = "This IP Address belongs to " + name + " Account " + str(number) + " " + subnetname + " " + j + " within Cargill Range"
        if not string:
            string = "This IP Address is not found within Cargill AWS Accounts of \n Dev(dev account number) \n Prod(prod account number) \n Stage(Stage account number) \n Shared(Shared account number)"
    else:
        string = "Not a valid IP Address"
     
    return {
        'statusCode': 200,
        'body': string
    }