import json
import boto3

def lambda_handler(event, context):
    client = boto3.client('elbv2', 'us-east-1')
    describeLB = client.describe_load_balancers()
    #print(describeLB)
    for lbs in (describeLB['LoadBalancers']):
        if "Scheme" in lbs:
            if (lbs['Scheme'] == "internet-facing" and "app/" in lbs['LoadBalancerArn']):
                print(lbs['LoadBalancerArn'])
                print(lbs['SecurityGroups'])
                SGlist = lbs['SecurityGroups']
                if 'sg-0aa31e33b508635dd' not in SGlist:
                    SGlist.append('sg-0aa31e33b508635dd')
                    print(SGlist)
                    print("---------------------------------------------------------------")
                    setSG = client.set_security_groups(
                        LoadBalancerArn=lbs['LoadBalancerArn'],
                        SecurityGroups=SGlist,
                    )
                    print(setSG)
                    print("---------------------------------------------------------------")
                else:
                    print("Security group already present")
                    print("---------------------------------------------------------------")