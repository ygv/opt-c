resource "aws_efs_file_system" "efs" {
    creation_token = "efs"
    performance_mode = "generalPurpose"
    throughput_mode = "bursting"
    encrypted = "true"
    tags = {
      Name = "${var.team}-${var.app}-${var.microservice}-efs"
    }
}

resource "aws_efs_mount_target" "efs-mt" {
    count = length(var.subnet_ids)
    file_system_id  = aws_efs_file_system.efs.id
    subnet_id = "${element(var.subnet_ids, count.index)}"
    security_groups = [var.cargill_managed_sg_id, var.common_dev_sg_id]
}

#resource "aws_efs_file_system" "efs1" {
#    creation_token = "efs1"
#    performance_mode = "generalPurpose"
#    throughput_mode = "bursting"
#    encrypted = "true"
#    tags = {
#      Name = "${var.team}-${var.app}-${var.microservice}-efs1"
#    }
#}

#resource "aws_efs_mount_target" "efs-mt1" {
#    count = length(var.subnet_ids)
#    file_system_id  = aws_efs_file_system.efs1.id
#    subnet_id = "${element(var.subnet_ids, count.index)}"
#    security_groups = [var.common_dev_sg_id]
#}

#resource "aws_efs_file_system" "efs2" {
#    creation_token = "efs2"
#    performance_mode = "generalPurpose"
#    throughput_mode = "bursting"
#    encrypted = "true"
#    tags = {
#      Name = "${var.team}-${var.app}-${var.microservice}-efs2"
#    }
#}

#resource "aws_efs_mount_target" "efs-mt2" {
#    count = length(var.subnet_ids)
#    file_system_id  = aws_efs_file_system.efs2.id
#    subnet_id = "${element(var.subnet_ids, count.index)}"
#    security_groups = [var.common_dev_sg_id]
#}

