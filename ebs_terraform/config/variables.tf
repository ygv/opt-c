variable "region" {
    default = ""
}
variable "vpc_id" {
    default = ""
}
variable "subnet_ids" {
    description = "EC2 subnets"
    type        = list
}

variable "common_dev_sg_id" {
    default = ""
}
variable "env" {
    default = ""
}
variable "app" {
    default = ""
}
variable "team" {
    default = ""
}
variable "account_id" {
    default = ""
    description = "AWS account id"
}
variable "microservice" {
      default = ""
}
