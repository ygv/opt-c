####################EFS Details######################
region                          = "us-east-1"
vpc_id                          = "vpc-0203ddc19e978da69"
#subnet_ids                      = ["subnet-0ee64768d5934db05" , "subnet-04a6fd06bff22f492" , "subnet-019474e140741d209"]  # public subnets
subnet_ids                      = ["subnet-0cb72a3634dacbb2e" , "subnet-019474e140741d209" , "subnet-0f83d46704a704fdb"]  # private subnets
team                            = "tgrc-eks"
app                             = "soar"
env                             = "dev"
microservice                    = "hive"
common_dev_sg_id                = "sg-0fcc675f8889ea1ff"
cargill_managed_sg_id           = "sg-0b6b9a622c643a0e7"
account_id                      = 248607199015
