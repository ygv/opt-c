import json
import boto3

def lambda_handler(event, context):
    # TODO implement
    SGs = []
    ec2 = boto3.client('ec2',region_name='us-east-1')
    response = ec2.describe_security_groups()
    for i in response['SecurityGroups']:
       print ("Security Group Name: "+i['GroupName'])
       print ("The Ingress rules are as follows: ")
       for j in i['IpPermissions']:
           try:
              for k in j['IpRanges']:
                  print ("IP Ranges: "+k['CidrIp'])
                  IpAddress = k['CidrIp']
                  if IpAddress == "0.0.0.0/0":
                     print ("Security Group Id: "+i['GroupId'])
                     SGs.append(i['GroupId'])
    
           except Exception:
              print ("No value for ip ranges available for this security group")
              continue
    print (SGs)
