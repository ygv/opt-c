# Launch Configuration
#------------------------------------------------------------------------------
resource "aws_launch_configuration" "lc" {
  name                             = "test_lc"
  image_id                         = "ami-0ed9277fb7eb570c9"
  instance_type                    = "t2.micro"
  iam_instance_profile             = "admin_role"
  key_name                         = "test-pair"
  security_groups                  = ["sg-0e6bbf9b8a6db2bb6"]
  associate_public_ip_address      = true
  #user_data                        = ""
  enable_monitoring                = true
  ebs_optimized                    = false
}

#------------------------------------------------------------------------------
# Auto Scaling Group
#------------------------------------------------------------------------------
resource "aws_autoscaling_group" "asg" {
  depends_on                = [aws_launch_configuration.lc]
  name                      = "test_asg"
  max_size                  = 2
  min_size                  = 1
  launch_configuration      = aws_launch_configuration.lc.name
  desired_capacity          = 1
  vpc_zone_identifier       = ["subnet-050cc84ce294bb785", "subnet-0a6d6880f0f123051", "subnet-01f2bbf318d8bc150"]
  protect_from_scale_in     = false
  tags = [
    {
      key                 = "Name"
      value               = "test_asg"
      propagate_at_launch = true
    },
  ]
}

#------------------------------------------------------------------------------
# AUTOSCALING POLICIES
#------------------------------------------------------------------------------
# Scaling UP - CPU High
resource "aws_autoscaling_policy" "cpu_high" {
  name                   = "test-cpu-high"
  autoscaling_group_name = aws_autoscaling_group.asg.name
  adjustment_type        = "ChangeInCapacity"
  policy_type            = "SimpleScaling"
  scaling_adjustment     = "1"
  cooldown               = "300"
}
# Scaling DOWN - CPU Low
resource "aws_autoscaling_policy" "cpu_low" {
  name                   = "test-cpu-high"
  autoscaling_group_name = aws_autoscaling_group.asg.name
  adjustment_type        = "ChangeInCapacity"
  policy_type            = "SimpleScaling"
  scaling_adjustment     = "-1"
  cooldown               = "300"
}

#------------------------------------------------------------------------------
# CLOUDWATCH METRIC ALARMS
#------------------------------------------------------------------------------
resource "aws_cloudwatch_metric_alarm" "cpu_high_alarm" {
  alarm_name          = "test-cpu-high-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "80"
  actions_enabled     = true
  alarm_actions       = ["${aws_autoscaling_policy.cpu_high.arn}"]
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.asg.name}"
  }
}
resource "aws_cloudwatch_metric_alarm" "cpu_low_alarm" {
  alarm_name          = "test-cpu-low-alarm"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "10"
  actions_enabled     = true
  alarm_actions       = ["${aws_autoscaling_policy.cpu_low.arn}"]
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.asg.name}"
  }
}
