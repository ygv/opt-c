import json
import boto3
from botocore.vendored import requests

sesclient = boto3.client("ses", region_name='us-east-1')
ec2 = boto3.client('ec2', region_name='us-east-1')
recipient_email = [ "chitra_s@cargill.com", "govindavaraprasad_yatam@cargill.com" ]

def send_email(subject, body, recipient_email):
    
    SOA_AUTH_URL = "https://alb-soa-dev-lwr-801363592.us-east-1.elb.amazonaws.com/auth/login"
    request_header = {"Content-Type": "application/json"}
    payload_data = {"username": "thehive-webhooks", "password": "*;3'FE=T'd*7"}
    response_get_token = requests.post(SOA_AUTH_URL, data=json.dumps(payload_data), headers=request_header, verify=False)
    auth_token = response_get_token.json()['access_token']
    SOA_EMAIL_URL = "https://alb-soa-dev-lwr-801363592.us-east-1.elb.amazonaws.com/email/send-email"
    request_header = {"Content-Type": "application/json", "Authorization": f"Bearer {auth_token}"}
    payload_data = {
        "email_subject": subject,
        "email_body": body,
        "email_recipients": recipient_email
    }
    mail_response = requests.post(SOA_EMAIL_URL, data=json.dumps(payload_data), headers=request_header, verify=False)
    print(mail_response.content)

def lambda_handler(event, context):
    # TODO implement
    response_one = ec2.describe_instances(Filters=[
        {
            'Name': 'instance-state-name',
            'Values': [
                'stopped'
            ]
        },
    ])

    #print(response_one)
    instance_idlist = []
    for reservation in response_one["Reservations"]:
        for instance in reservation["Instances"]:
            instance_idlist.append(instance["InstanceId"])
    print(instance_idlist)
    print("-----------------------------------------------------")

    instanceid_list = []
    instances = [i for i in boto3.resource('ec2', region_name='us-east-1').instances.all()]
    for i in instances:
        if 'ITOwnerEmail' not in [t['Key'] for t in i.tags]:
            #print(i.instance_id)
            instanceid_list.append(i.instance_id)
    print(instanceid_list)
    print("-----------------------------------------------------")

    response = ec2.describe_volumes(
        Filters=[
            {
                'Name': 'status',
                'Values': [
                    'available',
                ]
            },
        ]
    )
    #print(response)
    vol_idlist = []
    for vols in response["Volumes"]:
        vol_idlist.append(vols["VolumeId"])
    print(vol_idlist)

    if instance_idlist:
        print("Start sending email for users about stopped instances========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "<b>Hi Team,</b><br><br>"
        msg += "<b>Please find the  Stopped instance details in DEV-248607199015 Account:</b><br><br>"
        txt += "<table border='1' cellpadding='2'><tr><th bgcolor='#aaaaaa'><b>Instance ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th></tr>"
        for each_line in instance_idlist:
            txt +="<tr><td>" + each_line + "</td><td> Stopped </td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send stopped instances email")

            subject = 'Stopped instance details in [DEV-248607199015] Account'

            send_email(subject, msg, recipient_email)

    if instanceid_list:
        print("Start sending email for users about missing tag========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "<b>Hi Team,</b><br><br>"
        msg += "<b>Please find the Manually Create instance details in DEV-248607199015 Account:</b><br><br>"
        txt += "<table border='1' cellpadding='2'><tr><th bgcolor='#aaaaaa'><b>Instance ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th></tr>"
        for each_line in instanceid_list:
            txt +="<tr><td>" + each_line + "</td><td> Manual </td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send missing tag email")

            subject = 'Manually Create instance details in [DEV-248607199015] Account'

            send_email(subject, msg, recipient_email)


    if vol_idlist:
        print("Start sending email for users about available volumes========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "<b>Hi Team,</b><br><br>"
        msg += "<b>Please find the Available Volumes details in DEV-248607199015 Account which are not in use:</b><br><br>"
        txt += "<table border='1' cellpadding='2'><tr><th bgcolor='#aaaaaa'><b>Volume ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th></tr>"
        for each_line in vol_idlist:
            txt +="<tr><td>" + each_line + "</td><td> Available </td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send available volumes email")

            subject = 'Available Volumes in [DEV-248607199015] Account'

            send_email(subject, msg, recipient_email)