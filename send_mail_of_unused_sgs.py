import json
import boto3
from botocore.vendored import requests

sesclient = boto3.client("ses", region_name='us-east-1')
ec2 = boto3.client('ec2', region_name='us-east-1')
recipient_email = [ "govindavaraprasad_yatam@cargill.com" ]

def send_email(subject, body, recipient_email):
    
    SOA_AUTH_URL = "https://alb-soa-dev-lwr-801363592.us-east-1.elb.amazonaws.com/auth/login"
    request_header = {"Content-Type": "application/json"}
    payload_data = {"username": "thehive-webhooks", "password": "*;3'FE=T'd*7"}
    response_get_token = requests.post(SOA_AUTH_URL, data=json.dumps(payload_data), headers=request_header, verify=False)
    auth_token = response_get_token.json()['access_token']
    SOA_EMAIL_URL = "https://alb-soa-dev-lwr-801363592.us-east-1.elb.amazonaws.com/email/send-email"
    request_header = {"Content-Type": "application/json", "Authorization": f"Bearer {auth_token}"}
    payload_data = {
        "email_subject": subject,
        "email_body": body,
        "email_recipients": recipient_email
    }
    mail_response = requests.post(SOA_EMAIL_URL, data=json.dumps(payload_data), headers=request_header, verify=False)
    print(mail_response.content)

def getList(vol_idlist):
    return list(vol_idlist.keys())

def lambda_handler(event, context):
    # TODO implement
    ec2 = boto3.resource('ec2',region_name='us-east-1')
    sgs = ec2.security_groups.all()
    all_sgs = set([sg.group_name for sg in sgs])
    instances = ec2.instances.all()
    inssgs = set([sg['GroupName'] for ins in instances for sg in ins.security_groups])
    unused_sgs = all_sgs - inssgs
    unused_sgs_list = list(unused_sgs)
    print(unused_sgs_list)

    if unused_sgs_list:
        print("Start sending email for users about unused security groups========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "<b>Hi Team,</b><br><br>"
        msg += "<b>Please find the unused security groups in DEV-248607199015 Account:</b><br><br>"
        txt += "<table border='1' cellpadding='1'><tr><th bgcolor='#aaaaaa'><b>Security Group Name</b></th></tr>"
        for sg in unused_sgs_list:
            txt +="<tr><td>" + sg + "</td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send unused security groups email")

            subject = 'Unused Security Groups details in [DEV-248607199015] Account'

            send_email(subject, msg, recipient_email)