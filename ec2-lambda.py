import json
import boto3
sesclient = boto3.client("ses", region_name='us-east-1')
ec2 = boto3.client('ec2', region_name='us-east-1')

def lambda_handler(event, context):
    # TODO implement
    response_one = ec2.describe_instances(Filters=[
        {
            'Name': 'instance-state-name',
            'Values': [
                'stopped'
            ]
        },
    ])

    #print(response_one)
    instance_idlist = []
    for reservation in response_one["Reservations"]:
        for instance in reservation["Instances"]:
            instance_idlist.append(instance["InstanceId"])
    print(instance_idlist)
    print("-----------------------------------------------------")

    instanceid_list = []
    instances = [i for i in boto3.resource('ec2', region_name='us-east-1').instances.all()]
    for i in instances:
        if 'App' not in [t['Key'] for t in i.tags]:
            #print(i.instance_id)
            instanceid_list.append(i.instance_id)
    print(instanceid_list)
    print("-----------------------------------------------------")

    response = ec2.describe_volumes(
        Filters=[
            {
                'Name': 'status',
                'Values': [
                    'available',
                ]
            },
        ]
    )
    #print(response)
    vol_idlist = []
    for vols in response["Volumes"]:
        vol_idlist.append(vols["VolumeId"])
    print(vol_idlist)

    if instance_idlist:
        print("Start sending email for users about stopped instances========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "The following have Stopped Instances in Dev:<br><br>"
        txt += "<table border='1' cellpadding='2'><tr><th bgcolor='#aaaaaa'><b>Instance ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th></tr>"
        for each_line in instance_idlist:
            txt +="<tr><td>" + each_line + "</td><td> Stopped </td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send stopped instances email")

            subject = '[Testing] Stopped Instances'

            body = """
                <html>
                <body>
                    """+str(msg)+"""
                </body>
                </html>
            """

            From_Email = "durgajana2011@gmail.com"
            To_Email = "durgajana2011@gmail.com"
            html = body.format(msg)
            emailBody = html
            message = {"Subject": {"Data": subject}, "Body": {"Html": {"Data": emailBody}}}
            sesresponse_SI = sesclient.send_email(Source=From_Email, Destination={"ToAddresses": [To_Email]}, Message=message)
            #return sesresponse_SI
            #print(sesresponse)

    if instanceid_list:
        print("Start sending email for users about missing tag========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "The following have Missing Tag in Dev:<br><br>"
        txt += "<table border='1' cellpadding='2'><tr><th bgcolor='#aaaaaa'><b>Instance ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th></tr>"
        for each_line in instanceid_list:
            txt +="<tr><td>" + each_line + "</td><td> Manual </td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send missing tag email")

            subject = '[Testing] Missing Tag'

            body = """
                <html>
                <body>
                    """+str(msg)+"""
                </body>
                </html>
            """

            From_Email = "durgajana2011@gmail.com"
            To_Email = "durgajana2011@gmail.com"
            html = body.format(msg)
            emailBody = html
            message = {"Subject": {"Data": subject}, "Body": {"Html": {"Data": emailBody}}}
            sesresponse_MI = sesclient.send_email(Source=From_Email, Destination={"ToAddresses": [To_Email]}, Message=message)
            #return sesresponse_MI
            #print(sesresponse)


    if vol_idlist:
        print("Start sending email for users about available volumes========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "The following have Available Volumes in Dev:<br><br>"
        txt += "<table border='1' cellpadding='2'><tr><th bgcolor='#aaaaaa'><b>Volume ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th></tr>"
        for each_line in vol_idlist:
            txt +="<tr><td>" + each_line + "</td><td> Available </td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send available volumes email")

            subject = '[Testing] Available Volumes'

            body = """
                <html>
                <body>
                    """+str(msg)+"""
                </body>
                </html>
            """

            From_Email = "durgajana2011@gmail.com"
            To_Email = "durgajana2011@gmail.com"
            html = body.format(msg)
            emailBody = html
            message = {"Subject": {"Data": subject}, "Body": {"Html": {"Data": emailBody}}}
            sesresponse_AV = sesclient.send_email(Source=From_Email, Destination={"ToAddresses": [To_Email]}, Message=message)
            return sesresponse_AV
            #print(sesresponse)
