##########EKS Cluster##############
resource "aws_eks_cluster" "eks_cluster" {
    #name     = "${var.team}-${var.tag}-${var.app}-${var.env}-CLUSTER"
    name     = "${var.env}-${var.app}-${var.microservice}-cluster"
    role_arn = aws_iam_role.iam_role.arn
  
    vpc_config {
      subnet_ids = var.subnet_ids
      endpoint_private_access = true
      endpoint_public_access = true
    }
  
    tags = {
      Name = "${var.env}-${var.app}-${var.microservice}-cluster"
    }
}
  
resource "aws_launch_template" "node_eks_launch_template" {
      name = "${var.env}-${var.app}-${var.microservice}-cluster-node-launch-template"
      #vpc_security_group_ids = [var.app_sg_id, var.cargill_managed_sg_id, aws_eks_cluster.eks_cluster.vpc_config[0].cluster_security_group_id]
    
      image_id = var.ami
      instance_type = var.instance_type
      key_name = var.key_name
      user_data = base64encode(local.eks-node-private-userdata)
      tag_specifications {
        resource_type = "instance"
    
        tags = merge(local.tags, map("Name", "${var.env}-${var.app}-${var.microservice}-cluster-node"))
      }
      lifecycle {
        create_before_destroy=true
      }
}
  
resource "aws_eks_node_group" "node_eks_cluster" {
    cluster_name    = aws_eks_cluster.eks_cluster.name
    node_group_name = "${var.env}-${var.app}-${var.microservice}-cluster-node-group"
    node_role_arn   = aws_iam_role.eks_nodes_cluster_iam_role.arn
    subnet_ids      = var.subnet_ids
    tags = merge(local.tags, map("Name", "node_eks_cluster"))
    instance_types  = []
    scaling_config {
      desired_size = var.asg_desired_size
      max_size     = var.asg_max_size
      min_size     = var.asg_min_size
    }
    launch_template {
      name = aws_launch_template.node_eks_launch_template.name
      version = aws_launch_template.node_eks_launch_template.latest_version
    }
    lifecycle {
      ignore_changes = [scaling_config[0].desired_size]
    }
  
    # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
    # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
    depends_on = [
      aws_launch_template.node_eks_launch_template,
      #aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
      #aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
      #aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
      #aws_iam_role_policy_attachment.SecretsManagerRead-us-east-1,
    ]
}
