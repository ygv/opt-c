provider "aws" {
    region = "us-east-1"
    assume_role {
        role_arn = "arn:aws:iam::248607199015:role/eks_full_role"
    }
}

terraform {
    required_version = ">=0.12.6, < 1.14"
    required_providers {
        random = {
            source = "hashicorp/random"
            version = ">= 2.3.0"
        }
        null = {
            source = "hashicorp/null"
            version = "~> 2.1"
        }
        template = {
            source = "hashicorp/template"
            version = "~> 2.1"
        }
        aws = {
            source = "hashicorp/aws"
            version = ">= 2.7.0"
        }
    }
}
