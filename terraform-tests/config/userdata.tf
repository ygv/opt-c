locals {
    
    eks-node-private-userdata = <<USERDATA
#!/bin/bash
set -ex
B64_CLUSTER_CA='${aws_eks_cluster.eks_cluster.certificate_authority[0].data}'
API_SERVER_URL='${aws_eks_cluster.eks_cluster.endpoint}'
K8S_CLUSTER_DNS_IP=172.20.0.10
/etc/eks/bootstrap.sh '${aws_eks_cluster.eks_cluster.name}' --kubelet-extra-args '--node-labels=eks.amazonaws.com/nodegroup-image=${var.ami},eks.amazonaws.com/capacityType=ON_DEMAND,eks.amazonaws.com/nodegroup=${var.env}-${var.app}-${var.microservice}-cluster-node-group --max-pods=50' --b64-cluster-ca $B64_CLUSTER_CA --apiserver-endpoint $API_SERVER_URL --dns-cluster-ip $K8S_CLUSTER_DNS_IP --use-max-pods false
USERDATA

}
