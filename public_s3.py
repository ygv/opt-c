import json
import boto3
import sys
from botocore.exceptions import ClientError

def lambda_handler(event, context):
    # TODO implement
    try:
        # Create S3 client, describe buckets.
        s3client = boto3.client('s3')
        list_bucket_response = s3client.list_buckets()

        for bucket_dictionary in list_bucket_response['Buckets']:
            print(bucket_dictionary['Name'])

            response = s3client.put_public_access_block(Bucket=bucket_dictionary['Name'],
                PublicAccessBlockConfiguration={
                    'BlockPublicAcls': True,
                    'IgnorePublicAcls': True,
                    'BlockPublicPolicy': True,
                    'RestrictPublicBuckets': True
                }
            )

    except:
        err = 'access_public_buckets Failed!'
        for e in sys.exc_info():
            err += str(e)
        print(err)
        
################################################################################################

def lambda_handler(event, context):
    # TODO implement
    try:
        # Create S3 client, describe buckets.
        s3client = boto3.client('s3')
        list_bucket_response = s3client.list_buckets()

        for bucket_dictionary in list_bucket_response['Buckets']:
            if (bucket_dictionary['Name'].startswith('trgc-devops')) or (bucket_dictionary['Name'].startswith('trgc-ops')) or (bucket_dictionary['Name'].startswith('acap-')) or (bucket_dictionary['Name'].startswith('ngsiem')):
                print(bucket_dictionary['Name'])
                response = s3client.put_public_access_block(Bucket=bucket_dictionary['Name'],
                    PublicAccessBlockConfiguration={
                        'BlockPublicAcls': True,
                        'IgnorePublicAcls': True,
                        'BlockPublicPolicy': True,
                        'RestrictPublicBuckets': True
                    }
                )

    except:
        err = 'access_public_buckets Failed!'
        for e in sys.exc_info():
            err += str(e)
        print(err)
        
        
#########################################################################################################################

import json
import boto3
import sys
from botocore.exceptions import ClientError

def lambda_handler(event, context):
    # TODO implement
    try:
        # Create S3 client, describe buckets.
        s3client = boto3.client('s3')
        list_bucket_response = s3client.list_buckets()
        
        for bucket_dictionary in list_bucket_response['Buckets']:
            print(bucket_dictionary['Name'])
            
            tag_response = s3client.get_bucket_tagging(Bucket=bucket_dictionary['Name'])
            print(tag_response)
    except:
        err = 'describe_public_buckets Failed!'
        for e in sys.exc_info():
            err += str(e)
        print(err)
        
################################################################################################
import json
import boto3
import sys
from botocore.exceptions import ClientError

def lambda_handler(event, context):
    # TODO implement
    try:
        # Create S3 client, describe buckets.
        s3client = boto3.client('s3')
        list_bucket_response = s3client.list_buckets()
        
        for bucket_dictionary in list_bucket_response['Buckets']:
            if (not bucket_dictionary['Name'].startswith('trgc-devops')) and (not bucket_dictionary['Name'].startswith('jdn')):
                print(bucket_dictionary['Name'])
                response = s3client.put_public_access_block(Bucket=bucket_dictionary['Name'],
                    PublicAccessBlockConfiguration={
                        'BlockPublicAcls': True,
                        'IgnorePublicAcls': True,
                        'BlockPublicPolicy': True,
                        'RestrictPublicBuckets': True
                    }
                )
    
    except:
        err = 'access_public_buckets Failed!'
        for e in sys.exc_info():
            err += str(e)
        print(err)