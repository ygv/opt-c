##########EKS Cluster##############
resource "aws_eks_cluster" "eks_cluster" {
    name     = "${var.team}-${var.tag}-${var.app}-${var.env}-CLUSTER"
    role_arn = aws_iam_role.iam_role.arn
    vpc_config {
      subnet_ids = var.subnets
      #endpoint_private_access = true
      #endpoint_public_access = true

    }

    tags = {
      Name = "${var.team}-${var.tag}-${var.app}-${var.env}-CLUSTER"
    }
  }

resource "aws_launch_template" "node_eks_launch_template" {
      name = "${var.team}-${var.tag}-${var.app}-${var.env}-CLUSTER-NODE_launch_template"
      #count = var.asg_desired_size
      #vpc_security_group_ids = [var.app_sg_id, aws_eks_cluster.eks_cluster.vpc_config[0].cluster_security_group_id]
      #block_device_mappings {
      #  device_name = "/dev/xvda"
      #  ebs {
      #    volume_size = 30
      #    volume_type = "gp2"
      #  }
      #}

      image_id = var.ami
      instance_type = var.instance_type
      key_name = var.key_name
      #user_data            = base64encode(local.eks-node-private-userdata)
      tag_specifications {
        resource_type = "instance"
        tags = merge(local.tags, map("Name", "${var.team}-${var.tag}-${var.app}-${var.env}-CLUSTER-NODE"))
      }
      lifecycle {
        create_before_destroy=true
      }
 }

resource "aws_eks_node_group" "node_eks_cluster" {
    cluster_name    = aws_eks_cluster.eks_cluster.name
    node_group_name = "${var.team}-${var.tag}-${var.app}-${var.env}-CLUSTER-NODE-GROUP"
    node_role_arn   = aws_iam_role.eks_nodes_cluster_iam_role.arn
    subnet_ids      = var.subnets
    tags = merge(local.tags, map("Name", "node_eks_cluster"))
    #ami_type = var.ami
    #disk_size       = 50
    instance_types  = []
    scaling_config {
      desired_size = var.asg_desired_size
      max_size     = var.asg_max_size
      min_size     = var.asg_min_size
    }
    #remote_access {
    # ec2_ssh_key = "test-pair"
    #}

    launch_template {
      name = aws_launch_template.node_eks_launch_template.name
      version = aws_launch_template.node_eks_launch_template.latest_version
    }
    lifecycle {
      ignore_changes = [scaling_config[0].desired_size]
    }

    # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
    # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
    depends_on = [
      #aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
      #aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
      #aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
      aws_launch_template.node_eks_launch_template
    ]
  }
