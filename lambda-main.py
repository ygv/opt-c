import json
import boto3
client_waf = boto3.client('waf-regional', 'us-east-1')
listWACL = client_waf.list_web_acls()
sgid = 'sg-0e6bbf9b8a6db2bb6'
ruleid = 'da52c658-f9f7-4e93-aef9-304ce0c42735'

def get_token():
    return client_waf.get_change_token()['ChangeToken']

def lambda_handler(event, context):
    client_elb = boto3.client('elbv2', 'us-east-1')
    describeLB = client_elb.describe_load_balancers()
    #print(describeLB)
    for lbs in (describeLB['LoadBalancers']):
        if "Scheme" in lbs:
            if (lbs['Scheme'] == "internet-facing" and "app/" in lbs['LoadBalancerArn']):
                print(lbs['LoadBalancerArn'])
                print(lbs['SecurityGroups'])
                SGlist = lbs['SecurityGroups']
                if sgid not in SGlist:
                    SGlist.append(sgid)
                    print(SGlist)
                    print("---------------------------------------------------------------")
                    setSG = client_elb.set_security_groups(
                        LoadBalancerArn=lbs['LoadBalancerArn'],
                        SecurityGroups=SGlist,
                    )
                    print(setSG)
                    print("---------------------------------------------------------------")
                else:
                    print("Security group already present")
                    print("---------------------------------------------------------------")
    
    for wacls in (listWACL['WebACLs']):
        print(wacls['WebACLId'])
        getwacl = client_waf.get_web_acl(
            WebACLId=wacls['WebACLId']
        )
        #print(getwacl)
        listrules = []
        for rules in (getwacl['WebACL']['Rules']):
            listrules.append(rules['RuleId'])
        print(listrules)
        if ruleid not in listrules:
            updatewacl = client_waf.update_web_acl(
                WebACLId=wacls['WebACLId'],
                ChangeToken=get_token(),
                Updates=[
                    {
                        'Action': 'INSERT',
                        'ActivatedRule': {
                            'Priority': 10,
                            'RuleId': ruleid,
                            'Action': {
                                'Type': 'ALLOW'
                            }
                        }
                    },
                ],
            )
