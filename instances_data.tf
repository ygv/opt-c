data "aws_instances" "test" {
  instance_tags = {
    "Name" =  "${var.env}-${var.app}-${var.microservice}-cluster-master"
  }

  instance_state_names = ["running", "stopped"]
  depends_on           = [aws_autoscaling_group.asg]
}

output ids {
    value = data.aws_instances.test.ids
}
output ips {
    value = data.aws_instances.test.private_ips
}

data "aws_secretsmanager_secret" "sm" {
  name = "test-sm"
}

data "aws_secretsmanager_secret_version" "secret-version" {
  secret_id = data.aws_secretsmanager_secret.sm.id
}


output key {
    value = data.aws_secretsmanager_secret_version.secret-version.secret_string
}

resource "null_resource" "configure" {
  count = 1

  #Run accross all clientnode systems, built previously via count perhaps
  connection {
    user = "ec2-user"
    type = "ssh"
    private_key = data.aws_secretsmanager_secret_version.secret-version.secret_string
    host = "${element(data.aws_instances.test.private_ips, count.index)}"
  }

  provisioner "remote-exec" {
    # Run service restart on each node in the clutser
    inline = [
      "sudo yum update -y",
      "aws --version",
      "aws sts get-caller-identity"
    ]
  }
}
