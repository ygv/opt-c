import json
import boto3

def lambda_handler(event, context):
    # TODO implement
    SGs = []
    ec2 = boto3.client('ec2',region_name='us-east-1')
    response = ec2.describe_security_groups()
    print(response)
    
    for i in response['SecurityGroups']:
       print ("Security Group Name: "+i['GroupName'])
       print ("The Ingress rules are as follows: ")
       if i['GroupName'].startswith('k8s'):
            for j in i['IpPermissions']:
                try:
                    for k in j['IpRanges']:
                        print ("IP Ranges: "+k['CidrIp'])
                        IpAddress = k['CidrIp']
                        if IpAddress == "0.0.0.0/0":
                            print ("Security Group Id: "+i['GroupId'])
                            SGs.append(i['GroupId'])
    
                except Exception:
                    print ("No value for ip ranges available for this security group")
                    continue
    print (SGs)
    if SGs:
        SGs = list(set(SGs))
        for SG in SGs:
            sgr_response = ec2.describe_security_group_rules(
                Filters=[
                    {
                        'Name': 'group-id',
                        'Values': [
                            SG,
                        ]
                    },
                ],
            )
        
            #print(sgr_response)
            for x in sgr_response['SecurityGroupRules']:
                print(x)
                if x['CidrIpv4'] == "0.0.0.0/0" and x['IsEgress'] == False:
                    print(x['CidrIpv4'])
                    print(x['GroupId'])
                    try:
                        modifyresponse = ec2.modify_security_group_rules(
                            GroupId=x['GroupId'],
                            SecurityGroupRules=[
                                {
                                    'SecurityGroupRuleId': x['SecurityGroupRuleId'],
                                    'SecurityGroupRule': {
                                        'IpProtocol': x['IpProtocol'],
                                        'FromPort': x['FromPort'],
                                        'ToPort': x['ToPort'],
                                        'CidrIpv4': '10.0.0.0/8'
                                    }
                                },
                            ])
                    except Exception:
                        print('Could not modify security group rule.')
                        #raise
                    #else:
                    #    return modifyresponse
