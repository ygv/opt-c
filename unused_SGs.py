import json
import boto3

def lambda_handler(event, context):
    # TODO implement
    ec2 = boto3.resource('ec2',region_name='us-east-1')
    sgs = ec2.security_groups.all()
    all_sgs = set([sg.group_name for sg in sgs])
    instances = ec2.instances.all()
    inssgs = set([sg['GroupName'] for ins in instances for sg in ins.security_groups])
    unused_sgs = all_sgs - inssgs
    for sg in unused_sgs:
       print(sg)