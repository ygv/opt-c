import json
import boto3
client = boto3.client('waf-regional', 'us-east-1')
listWACL = client.list_web_acls()
#print(listWACL)

def get_token():
    return client.get_change_token()['ChangeToken']

for wacls in (listWACL['WebACLs']):
    print(wacls['WebACLId'])
    getwacl = client.get_web_acl(
        WebACLId=wacls['WebACLId']
    )
    #print(getwacl)
    listrules = []
    ruleid = 'ef6003d4-7085-428d-b5d0-7993d6ea763c'
    for rules in (getwacl['WebACL']['Rules']):
        listrules.append(rules['RuleId'])
    print(listrules)
    if ruleid not in listrules:
        updatewacl = client.update_web_acl(
            WebACLId=wacls['WebACLId'],
            ChangeToken=get_token(),
            Updates=[
                {
                    'Action': 'INSERT',
                    'ActivatedRule': {
                        'Priority': 10,
                        'RuleId': ruleid,
                        'Action': {
                            'Type': 'ALLOW'
                        }
                    }
                },
            ],
            #DefaultAction={
               # 'Type': 'ALLOW'
            #}
        )