./test.sh filename
#!/bin/bash

file=$1
while read line
do 
echo $line
done < $file

########################################################################################################

./node_scaling.sh filename
#!/bin/bash

file=$1
aws s3 cp s3://bucket/folder/$file <path/of/file/to/copy>

while read line
do 
echo $line

node_name=`aws eks list-nodegroups --cluster-name $line --region us-east-1 --output text | awk '{print $2}' | awk 'END{print}'`
echo $node_name
desired_count=`eksctl get nodegroup --cluster $line --name $node_name --region us-east-1 | awk '{print $7}' | awk 'END{print}'`
echo $desired_count
if [ $desired_count -gt 3 ]
then
    aws eks --region us-east-1 update-kubeconfig --name $line
    pod_count=`kubectl get replicaset -n kube-system | grep logstash | awk '{print $2}' | awk 'END{print}'`
    echo $pod_count
    if [ $pod_count -le 9 ]
    then
        eksctl scale nodegroup --cluster $line --region us-east-1 --nodes 3 --name $node_name
        echo "Scaled to 3 nodes"
        
    elif [ $pod_count -gt 9 ] && [ $pod_count -le 12 ]
    then
        if [ $desired_count -eq 4 ]
            echo "4 nodes and no need to scale"
        else
            eksctl scale nodegroup --cluster $line --region us-east-1 --nodes 4 --name $node_name
            echo "Scaled to 4 nodes"
        fi

    elif [ $pod_count -gt 12 ] && [ $pod_count -le 15 ]
    then
        if [ $desired_count -eq 5 ]
            echo "5 nodes and no need to scale"
        else
            eksctl scale nodegroup --cluster $line --region us-east-1 --nodes 5 --name $node_name
        fi
    elif [ $pod_count -gt 15 ] && [ $pod_count -le 18 ]
    then
        if [ $desired_count -eq 6 ]
            echo "6 nodes and no need to scale"
        else
            eksctl scale nodegroup --cluster $line --region us-east-1 --nodes 6 --name $node_name
        fi
    elif [ $pod_count -gt 18 ] && [ $pod_count -le 21 ]
    then
        if [ $desired_count -eq 7 ]
            echo "7 nodes and no need to scale"
        else
            eksctl scale nodegroup --cluster $line --region us-east-1 --nodes 7 --name $node_name
        fi
    elif [ $pod_count -gt 21 ] && [ $pod_count -le 24 ]
    then
        if [ $desired_count -eq 8 ]
            echo "8 nodes and no need to scale"
        else
            eksctl scale nodegroup --cluster $line --region us-east-1 --nodes 8 --name $node_name
        fi
    fi
else
    echo "3 nodes and no need to scale"
fi
done < $file
