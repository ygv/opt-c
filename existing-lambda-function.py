import json
import boto3
from botocore.vendored import requests

sesclient = boto3.client("ses", region_name='us-east-1')
ec2 = boto3.client('ec2', region_name='us-east-1')
recipient_email = [ "govindavaraprasad_yatam@cargill.com" ]

def send_email(subject, body, recipient_email):
    
    SOA_AUTH_URL = "https://alb-soa-dev-lwr-801363592.us-east-1.elb.amazonaws.com/auth/login"
    request_header = {"Content-Type": "application/json"}
    payload_data = {"username": "thehive-webhooks", "password": "*;3'FE=T'd*7"}
    response_get_token = requests.post(SOA_AUTH_URL, data=json.dumps(payload_data), headers=request_header, verify=False)
    auth_token = response_get_token.json()['access_token']
    SOA_EMAIL_URL = "https://alb-soa-dev-lwr-801363592.us-east-1.elb.amazonaws.com/email/send-email"
    request_header = {"Content-Type": "application/json", "Authorization": f"Bearer {auth_token}"}
    payload_data = {
        "email_subject": subject,
        "email_body": body,
        "email_recipients": recipient_email
    }
    mail_response = requests.post(SOA_EMAIL_URL, data=json.dumps(payload_data), headers=request_header, verify=False)
    print(mail_response.content)

def getList(vol_idlist):
    return list(vol_idlist.keys())

def lambda_handler(event, context):
    # TODO implement
    response_one = ec2.describe_instances(Filters=[
        {
            'Name': 'instance-state-name',
            'Values': [
                'stopped'
            ]
        },
    ])

    #print(response_one)
    instance_idlist = {}
    for reservation in response_one["Reservations"]:
        for instance in reservation["Instances"]:
            instance_list = []
            instance_id = (instance["InstanceId"])
            instance_type = (instance['InstanceType'])
            key = instance["Tags"]
            def search_value(name):
                for keyval in key:
                    if name.lower() == keyval['Key'].lower():
                        return keyval['Value']
                return "None"
            
            instancenamekey = 'Name'
            instanceownerkey = 'ITOwnerEmail'
            if ((search_value(instancenamekey) != None) or (search_value(instanceownerkey) != None)) :
                instance_name = search_value(instancenamekey)
                instance_owner = search_value(instanceownerkey)
                instance_list.extend([instance_name, 'stopped', instance_owner, instance_type])    
            else:
                instance_name = search_value(instancenamekey)
                instance_owner = search_value(instanceownerkey)
                instance_list.extend([instance_name, 'stopped', instance_owner, instance_type])    

            instance_idlist[instance_id] = instance_list

    print(instance_idlist)
    print("-----------------------------------------------------")

    #instanceid_list = []
    #instances = [i for i in boto3.resource('ec2', region_name='us-east-1').instances.all()]
    #for i in instances:
    #    if 'ITOwnerEmail' not in [t['Key'] for t in i.tags]:
    #        #print(i.instance_id)
    #        instanceid_list.append(i.instance_id)
    #print(instanceid_list)
    #print("-----------------------------------------------------")

    response = ec2.describe_volumes(
        Filters=[
            {
                'Name': 'status',
                'Values': [
                    'available',
                ]
            },
        ]
    )
    #print(response)
    vol_idlist = {}
    for vols in response["Volumes"]:
        volume_list = []
        volume_id = (vols["VolumeId"])
        volume_size = (vols["Size"])
        volume_list.extend([volume_id, 'available'])
        if volume_size in vol_idlist.keys():
            vol_idlist[volume_size].append(volume_list)
        else:
            vol_idlist[volume_size] = [volume_list]

    print(vol_idlist)
    size_sort = getList(vol_idlist)
    size_sort.sort(reverse = True)
    print(size_sort)

    if instance_idlist or vol_idlist:
        print("Start sending email for users about stopped instances and available volumes========")
        msg = ''
        txt = ''
        msg = "<b>Automatically generated e-mail - please do not reply.</b><br><br>"
        msg += "<b>Hi Team,</b><br><br>"
        msg += "<b>Please find the Stopped instance details in DEV-248607199015 Account:</b><br><br>"
        txt += "<table border='1' cellpadding='5'><tr><th bgcolor='#aaaaaa'><b>Name</b></th><th bgcolor='#aaaaaa'><b>Instance ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th><th bgcolor='#aaaaaa'><b>Owner</b></th><th bgcolor='#aaaaaa'><b>Size</b></th></tr>"
        for key, val in instance_idlist.items():
            txt +="<tr><td>" + val[0] + "</td><td>" + key + "</td><td>" + val[1] + "</td><td>" + val[2] + "</td><td>" + val[3] + "</td></tr>"
        txt += "</table> <br>"

        txt += "<b>Available Volumes details in DEV-248607199015 Account which are not in use:</b><br><br>"
        txt += "<table border='1' cellpadding='3'><tr><th bgcolor='#aaaaaa'><b>Volume ID</b></th><th bgcolor='#aaaaaa'><b>Status</b></th><th bgcolor='#aaaaaa'><b>Size</b></th></tr>"
        for x in size_sort:
            for y in vol_idlist[x]:
                #y = vol_idlist[x]
                print(y)
                str_x = str(x)
                txt +="<tr><td>" + y[0] + "</td><td>" + y[1] + "</td><td>" + str_x + "</td></tr>"
        txt += "</table> <br>"

        if txt != '' :
            msg += txt
            msg += "<br>Thank you!"
            print("Send stopped instances and available volumes email")

            subject = 'Stopped instances and Available Volumes details in [DEV-248607199015] Account'

            send_email(subject, msg, recipient_email)