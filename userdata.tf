locals {
    
    eks-node-private-userdata = <<USERDATA
#!/bin/bash
set -ex
B64_CLUSTER_CA='${aws_eks_cluster.eks_cluster.certificate_authority[0].data}'
API_SERVER_URL='${aws_eks_cluster.eks_cluster.endpoint}'
K8S_CLUSTER_DNS_IP=172.20.0.10
/etc/eks/bootstrap.sh '${aws_eks_cluster.eks_cluster.name}' --kubelet-extra-args '--node-labels=eks.amazonaws.com/nodegroup-image=${var.ami},eks.amazonaws.com/capacityType=ON_DEMAND,eks.amazonaws.com/nodegroup=${var.env}-${var.app}-${var.microservice}-cluster-node-group --max-pods=50' --b64-cluster-ca $B64_CLUSTER_CA --apiserver-endpoint $API_SERVER_URL --dns-cluster-ip $K8S_CLUSTER_DNS_IP --use-max-pods false
sed -i 's/"Hard": 200,/"Hard": 65535,/' /etc/docker/daemon.json
sed -i 's/"Soft": 100/"Soft": 65535/' /etc/docker/daemon.json
sed -i 's/"Hard": 2048,/"Hard": 4096,/' /etc/docker/daemon.json
sed -i 's/"Soft": 1024/"Soft": 4096/' /etc/docker/daemon.json
sed -i 's/"log-driver": "json-file",/"log-driver": "awslogs",/' /etc/docker/daemon.json
sed -i 's/"max-size": "10m",/"awslogs-create-group": "true",/' /etc/docker/daemon.json
sed -i 's/"max-file": "5"/"awslogs-group": "dev-soar-ns-cluster",/' /etc/docker/daemon.json
sed -i '/"awslogs-group"/a "awslogs-region": "us-east-1",' /etc/docker/daemon.json
sed -i '/"awslogs-region"/a "awslogs-stream": "ns-docker-log"' /etc/docker/daemon.json
service docker restart

USERDATA

}